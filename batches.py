import datetime
import sys
import os
import pandas as pd
from threading import Thread
from batch_processor import BatchProcessor

class BatchProcessCSV:

    def __init__(self, data_csv_file):
        self.data_csv_file= data_csv_file
        self.processor = BatchProcessor(self.batch_func, worker_num=2, batch_size=10)
        self.threads = []

    def batch_func(self, batch):
        return [v + v for v in batch]

    def doBatchAction(self):
        startTime=datetime.datetime.now()
        print("begin processing ...")
        
        # calling the readcsvdata cells
        self.readcsvdata()
        endTime=datetime.datetime.now()
        print ("process is completed")
        print(endTime, startTime)


    def readcsvdata(self):

        for i in self.data_csv_file.index:
            t = Thread(target=self.validate_input, args=(i, self.processor))
            t.start()
            self.threads.append(t)
        for t in self.threads:
            t.join()
    
    def validate_input(self, data, processor):
        # validate the cell data here
        print(data, processor.process(data))

 
#process
if __name__=="__main__":

    df = pd.read_csv('Test.csv', index_col=0)

    csvProcess = BatchProcessCSV(df)
    csvProcess.doBatchAction()